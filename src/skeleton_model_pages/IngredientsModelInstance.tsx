export default interface IngredientModelInstance {
    recipes: any[],
    title: string,
    id: number,
    src: string,
    aisle: string,
    calories: number,
  }